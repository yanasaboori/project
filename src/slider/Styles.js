import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  root: {
    width: "90%",
    height: "800px",
  },
  img: {
    width: "100%",
    height: "800px",

    [theme.breakpoints.down("sm")]: {
      maxHeight: "400px",
    },
    [theme.breakpoints.down("xs")]: {
      maxHeight: "300px",
    },
    // maxHeight: "100%",
  },
}));
