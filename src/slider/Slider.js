import React from "react";
import useStyle from "./Styles";
import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";
import Grid from "@material-ui/core/Grid";
import dinamik from "../images/dinamik.jpg";
import bootax from "../images/bootax.jpg";
export default function Sliders() {
  const responsive = {
    0: {
      items: 1,
    },
    600: {
      items: 1,
    },
    1024: {
      items: 1,
    },
  };

  let classes = useStyle();
  return (
    <Grid item container>
      <AliceCarousel
        responsive={responsive}
        fadeOutAnimation
        dotsDisabled
        autoPlay
        infinite
        buttonsDisabled
        mouseDragEnabled
        autoPlayDirection="rtl"
        autoPlayInterval={2000}
      >
        <img className={classes.img} src={dinamik} alt="" />
        <img className={classes.img} src={bootax} alt="" />
      </AliceCarousel>
    </Grid>
  );
}
