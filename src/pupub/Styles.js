import { makeStyles } from "@material-ui/core/styles";
import background from "../images/backgroundPupub.jpg";

export default makeStyles((theme) => ({
  root: {
    width: "100%",
    height: "100%",
    zIndex: 5000,
    position: "fixed",
    top: 0,
    right: 0,
    backgroundColor: "rgba(159, 162, 166,0.9)",
    justifyContent: "center",
  },

  root2: {},
  contAll: {
    height: "300px",
    background: ` url(${background}) no-repeat center `,
    backgroundSize: "cover",
    borderRadius: "25px 5px 25px 5px",
    [theme.breakpoints.down("xs")]: {
      height: "253px",
    },
  },

  contLogPic: {
    height: "100%",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    "& img": {
      height: "180px",
      width: "100%",
    },
  },
  iconClose: {
    marginRight: "10px",
    marginTop: "10px",
  },
  titleInputs: {
    marginTop: "10px",
  },

  styleInputs: {
    outline: "none",
    border: "2px solid #6a6464",
    borderRadius: "15px",
    textAlign: "right",
    width: "100%",
    background: "transparent",
    height: "35px",
  },

  div: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-Between",
    height: "130px",
    marginTop: "20px",
    "& #id": {
      width: "50px",
    },
  },

  containerButtons: {
    position: "relative",
    top: "80px",
  },
}));
