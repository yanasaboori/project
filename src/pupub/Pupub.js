import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import React from "react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import Input from "../globalComponents/input/Input";
//img
import logoMoon from "../images/logoMoon.png";
import useStyles from "./Styles";

export default function Pupup({
  handleModal,
  array,
  setArray,
  name,
  setName,
  actors,
  setActors,
  director,
  setDirector,
  id,
  setId,
  getIndex,
}) {
  function changeName(e) {
    setName(e.target.value);
  }
  function changeActors(e) {
    setActors(e.target.value);
  }
  function changeDirector(e) {
    setDirector(e.target.value);
  }
  function changeId(e) {
    setId(e.target.value);
  }
  function insertData(e) {
    let copyArrat = [...array];
    copyArrat.push({ name, director, actors, id });
    setArray(copyArrat);
    handleModal();
  }
  function handleEdit() {
    handleModal();
    let copyArray = [...array];
    copyArray.splice(getIndex, 1, { name, director, actors, id });
    console.log(getIndex);
    setArray(copyArray);
  }
  function cancel() {
    handleModal();
  }
  const classes = useStyles();
  return (
    <Grid
      container
      justifyContent="center"
      alignItems="center"
      className={classes.root}
    >
      <Grid
        item
        container
        justifyContent="space-around"
        xl={7}
        lg={7}
        md={10}
        sm={11}
        xs={11}
        className={classes.root2}
      >
        <Grid
          container
          className={classes.contAll}
          justifyContent="space-between"
        >
          {/* //column1*/}
          <Grid
            item
            container
            xl={4}
            lg={4}
            md={5}
            sm={3}
            xs={3}
            className={classes.contLogPic}
          >
            <Grid item>
              <h4>بلک مون</h4>{" "}
            </Grid>
            <Grid item>
              <img src={logoMoon} className={classes.img} alt="" />
            </Grid>
          </Grid>
          {/* //column2 */}

          <Grid item container xl={8} lg={8} md={7} sm={9} xs={9}>
            <Grid item container justifyContent="flex-end">
              <h4> لطفا اطلاعات را وارد نمائید </h4>&nbsp; &nbsp; &nbsp;&nbsp;
              <AiOutlineCloseCircle
                size="30px"
                className={classes.iconClose}
                onClick={handleModal}
              />
            </Grid>
            <Input
              title1=": کارگردان"
              value1={director}
              change1={changeDirector}
              title2=": نام"
              value2={name}
              change2={changeName}
            />
            <Input
              title1=" : id   "
              value1={id}
              change1={changeId}
              title2=":بازیگر"
              value2={actors}
              change2={changeActors}
            />
            <Grid item container alignItems="center">
              <Grid item xl={3} lg={3} md={3} sm={4} xs={5}>
                <Button variant="contained" onClick={cancel}>
                  لغو
                </Button>
              </Grid>
              <Grid item xl={3} lg={3} md={3} sm={4} xs={5}>
                {getIndex === false ? (
                  <Button
                    variant="contained"
                    onClick={insertData}
                    disabled={
                      name === "" &&
                      actors === "" &&
                      director === "" &&
                      id === ""
                    }
                  >
                    درج
                  </Button>
                ) : (
                  <Button variant="contained" onClick={handleEdit}>
                    ویرایش
                  </Button>
                )}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
