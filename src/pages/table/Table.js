import React, { useState } from "react";
// component
import Pupub from "../../pupub/Pupub";
//matrial
import Fab from "@material-ui/core/Fab";
import Grid from "@material-ui/core/Grid";
// react icon
import { GoPlusSmall } from "react-icons/go";
import { AiOutlineEdit } from "react-icons/ai";
import { MdCopyAll, MdOutlineDeleteForever } from "react-icons/md";
// css
import useStyles from "./Style";
let arrayInite = [
  { name: "زخم", director: "مهدی فخیم زاده", actors: "محمد رضا", id: 1 },
  { name: "تک خال", director: "جوادعزتی", actors: "بهرام افشاری", id: 2 },
  { name: "نوروز", director: "محسن تنابنده", actors: "هماسعادت", id: 3 },
  { name: "سرنوشت", director: "رضاعطلاران", actors: "الناز", id: 4 },
  { name: "روزی", director: "اریا", actors: "جوادعزتی", id: 5 },
];

export default function Table() {
  const [showModal, setShowModal] = useState(false);
  const [array, setArray] = useState(arrayInite);
  const [name, setName] = useState("");
  const [director, setDirector] = useState("");
  const [actors, setActors] = useState("");
  const [id, setId] = useState("");
  const [getIndex, getSetIndex] = useState(false);

  function showModalInsert() {
    setShowModal(!showModal);
    setName("");
    setDirector("");
    setActors("");
    setId("");
    getSetIndex(false);
  }
  function handleDelete(index) {
    let copyArray = [...array];
    copyArray.splice(index, 1);
    setArray(copyArray);
  }

  function handleEdit(item, index) {
    showModalInsert();
    setName(item.name);
    setDirector(item.director);
    setActors(item.actors);
    setId(item.id);
    getSetIndex(index);
  }

  const classes = useStyles();
  return (
    <Grid container justifyContent="center" className={classes.root}>
      <Grid
        item
        container
        xl={11}
        lg={11}
        md={11}
        sm={11}
        xs={11}
        className={classes.root2}
      >
        <Grid item container justifyContent="flex-end" alignItems="center">
          <Fab
            variant="extended"
            size="medium"
            aria-label="add"
            className={classes.margin}
            onClick={showModalInsert}
          >
            درج اطلاعات
            <GoPlusSmall size="28px" />
          </Fab>
        </Grid>
        <Grid item container justifyContent="center">
          {" "}
          {array.length === 0 ? (
            <p className={classes.textError}>داده ای برای نمایش نیست </p>
          ) : (
            <Grid
              item
              container
              xl={7}
              lg={7}
              md={9}
              sm={11}
              xs={12}
              className={classes.contTable}
            >
              {" "}
              <table className={classes.table}>
                <tr className={classes.title}>
                  <th> عملیات</th>
                  <th>شماره شناسه</th>
                  <th>بازیگران</th>
                  <th>کارگردان</th>
                  <th>نام</th>
                </tr>
                {array.map((item, index) => {
                  return (
                    <tr>
                      <td>
                        <Grid container>
                          <Grid item xs={5}>
                            {" "}
                            <MdOutlineDeleteForever
                              size="25px"
                              onClick={() => handleDelete(index)}
                            />
                          </Grid>
                          <Grid item xs={5}>
                            {" "}
                            <AiOutlineEdit
                              size="25px"
                              onClick={() => handleEdit(item, index)}
                            />
                          </Grid>
                        </Grid>
                      </td>
                      <td>{item.id}</td>
                      <td>{item.actors}</td>
                      <td>{item.director}</td>
                      <td>{item.name}</td>
                    </tr>
                  );
                })}
              </table>
            </Grid>
          )}
        </Grid>
      </Grid>

      {showModal && (
        <Pupub
          handleModal={showModalInsert}
          array={array}
          setArray={setArray}
          name={name}
          setName={setName}
          actors={actors}
          setActors={setActors}
          director={director}
          setDirector={setDirector}
          id={id}
          setId={setId}
          getIndex={getIndex}
        />
      )}
    </Grid>
  );
}
