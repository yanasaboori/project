import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  root: {
    marginTop: "60px",
    width: "100%",
    height: "100vh",
    paddingBottom: "50px",
  },
  root2: {
    height: "457px",
    alignItems: "center",
  },

  contTable: {
    overflow: "auto",
    height: "auto",
    maxHeight: "400px",
  },
  table: {
    width: "100%",
    minWidth: "360px",
    border: "1px solid gray",
    borderCollapse: "collapse",
    backgroundColor: "white",
    "& tr td": {
      border: "1px solid #de9f0b",
    },
    "& td": {
      color: "black",
      width: "20px",
      textAlign: "center",
      [theme.breakpoints.down("xs")]: {
        fontSize: "11px",
      },
    },
  },

  title: {
    height: "50px",
    backgroundColor: "gray",
    color: "white",
  },
  textError: {
    color: "white",
  },
}));
