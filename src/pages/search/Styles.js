import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  root: {
    marginTop: "50px",
    minHeight: "100vh",
  },
  containericons: {
    display: "flex",
    justifyContent: "space-around",
  },
  divTabs: {
    paddingTop: "25px",
    width: "100%",
  },
  divInput: {
    boxShadow: "5px 2px 10px #b3b5b4",
    borderRadius: "3px",
  },
  inputValue: {
    color: "white",
    width: "90%",
    maxWidth: "75%",
    height: "35px",
    textAlign: "right",
    outline: "none",
    backgroundColor: "black",
    border: "none",
    borderBottom: "2px solid white ",
    [theme.breakpoints.down("xs")]: {
      maxWidth: "66%",
    },
  },

  containerInputApplay: {
    paddingTop: "20px",
  },
  img: {
    borderRadius: "10px",
    "&:hover": {
      border: "1px solid #de9f0b",
    },
  },
  textname: {
    textAlign: "center",
  },
}));
