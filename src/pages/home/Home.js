import React, { useState } from "react";
import rozi from "../../images/rozi.jpg";
import { cro1, cro2, pictureC } from "../../globalObjects/gloalObject";

// componnets
import ComponentImg from "../../globalComponents/componentImg/ComponentImg";
import CarouselCover from "../../globalComponents/carouselComponent/carouselCover/CarouselCover";
import CarouselSample from "../../globalComponents/carouselComponent/carouselsample/CrouselSmple";
import CarouselLarge from "../../globalComponents/carouselLarge/CarouselLarge";
// object

// css
export default function Home() {
  return (
    <>
      <ComponentImg src={rozi} />
      <CarouselSample
        showAll=" کمدی"
        newFilm="جدیدترین فیلم ها"
        arrayCarousel={cro1}
      />
      <CarouselLarge arrayObject={pictureC} />
      <CarouselCover newFilm="جدیدترین فیلم ها" arrayCarousel={cro2} />
      <CarouselSample arrayCarousel={cro1} />
      <CarouselCover newFilm="تخیلی" arrayCarousel={cro2} />
      <CarouselSample
        showAll=" پربازدیدترین"
        newFilm="جدیدترین فیلم ها "
        arrayCarousel={cro1}
      />
      <CarouselCover
        showAll="  جذاب ترین فیلم ها"
        newFilm=" فیلم های قدیمی تر"
        arrayCarousel={cro2}
      />
    </>
  );
}
