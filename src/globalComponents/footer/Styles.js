import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  root: {
    height: "164px",
    borderTop: "2px",
    borderTopStyle: "solid",
  },
  containerIcons: {
    height: "165px",
    [theme.breakpoints.down("sm")]: {
      alignItems: "flex-start",
    },
  },

  styleLink: {
    color: "#de9f0b",
    [theme.breakpoints.down("xs")]: { fontSize: "12px" },
    textDecoration: "none",
    fontSize: "13px",
    fontWeight: "500",
  },
  linkFooter: {
    paddingTop: "10px",
  },
}));
