import React from "react";
import { Paper } from "@material-ui/core";
import useStyles from "./Styles";
export default function Account({ nameAccount }) {
  let classes = useStyles();
  return (
    <Paper elevation={1} className={classes.root}>
      <h3 className={classes.heightTag}> {nameAccount} </h3>
      <div>
        <h5 className={classes.heightTag}> 1</h5>
        <h5 className={classes.heightTag}> 2</h5>
        <h5 className={classes.heightTag}> 3</h5>
        <h5 className={classes.heightTag}> 4</h5>
      </div>
    </Paper>
  );
}
