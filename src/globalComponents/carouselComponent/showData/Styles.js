import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  root: {
    marginBottom: "25px",
    boxShadow: " 0px 5px 15px 10px lightBlue",
  },
  itemImg: {
    maxHeight: "320px",
  },
  img: {
    width: "100%",
    maxHeight: "300px",
    [theme.breakpoints.down("xs")]: {
      maxHeight: "190px",
    },
  },
  textALeft: {
    textAlign: "left",
  },
  containerItemInfo: {
    backgroundColor: "white",
  },
  itemsInfo: {
    marginRight: "42px",
  },
  kkk: {
    width: "100%",
  },
}));
