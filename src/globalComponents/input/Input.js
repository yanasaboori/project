import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import useStyles from "./Styles";

export default function Input({
  value1,
  value2,
  change1,
  change2,
  title1,
  title2,
}) {
  //css
  const classes = useStyles();
  return (
    <Grid item container justifyContent="flex-end">
      <Grid
        item
        container
        justifyContent="space-between"
        xl={6}
        lg={6}
        sm={6}
        xs={6}
      >
        <Grid item xl={8} lg={8} md={8} sm={8} xs={7}>
          <input
            typr="text"
            value={value1}
            onChange={change1}
            className={classes.styleInputs}
          />
        </Grid>
        <Grid item xl={4} lg={4} sm={4} xs={5}>
          <p className={classes.styleText}>{title1} </p>
        </Grid>
      </Grid>
      <Grid
        item
        container
        xl={6}
        lg={6}
        sm={6}
        xs={6}
        justifyContent="space-around"
      >
        <Grid item xl={8} lg={8} md={8} sm={8} xs={8}>
          <input
            type="text"
            value={value2}
            onChange={change2}
            className={classes.styleInputs}
          />
        </Grid>
        <Grid item xl={4} lg={4} sm={4} xs={4}>
          <p className={classes.styleText}>{title2}</p>
        </Grid>
      </Grid>
    </Grid>
  );
}
