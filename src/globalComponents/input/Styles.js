import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  root: {
    position: "relative",
  },
  styleInputs: {
    border: "2px solid #6a6464",
    borderRadius: "15px",
    textAlign: "right",
    width: "100%",
    background: "transparent",
    height: "35px",
    color: "white",
    outlineColor: "#de9f0b",
    outlineWidth: "1px",
    [theme.breakpoints.down("xs")]: {
      height: "25px",
    },
  },
  styleText: {
    textAlign: "center",

    [theme.breakpoints.down("xs")]: {
      fontSize: "10px",
    },
  },
}));
