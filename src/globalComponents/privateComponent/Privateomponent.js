import React from "react";
import { useNavigate, Navigate } from "react-router-dom";
import { useUserState } from "../../context/useContext";
export default function PrivateComonent({ children }) {
  let isAuthenticate = useUserState();
  if (isAuthenticate.isAuthenticated) {
    return children;
  } else {
    return <Navigate to="/login" replace />;
  }
}
