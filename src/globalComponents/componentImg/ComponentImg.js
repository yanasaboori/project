import React from "react";
import useStyle from "./Styles";

export default function ComponentImg({ src }) {
  let classes = useStyle();
  return (
    <div className={classes.root}>
      <img src={src} className={classes.img} />
    </div>
  );
}
