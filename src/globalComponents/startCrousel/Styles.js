import { makeStyles } from "@material-ui//core/styles";

export default makeStyles((theme) => ({
  root2: {
    position: "relative",
  },
  imgs: {
    borderRadius: "62px",
    width: "132px",
    height: "125px",
  },
  divRow1: {
    width: "97%",
    display: "flex",
    justifyContent: "flex-end",
  },
  divImg: {
    width: "189px",
    height: "260px",
    backgroundColor: "lightblue",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "10px",
  },
}));
