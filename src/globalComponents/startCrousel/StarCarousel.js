import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import AliceCarousel from "react-alice-carousel";
import Grid from "@material-ui/core/Grid";

// css
import "react-alice-carousel/lib/alice-carousel.css";
import useStyles from "./Styles";

export default function StartCarousel({ newFilm, nameArtist, arrayCarousel }) {
  let classes = useStyles();
  const responsive = {
    0: {
      items: 2,
    },
    600: {
      items: 3,
    },
    1024: {
      items: 6,
    },
  };

  return (
    <Grid container justifyContent="center">
      <div className={classes.divRow1}>
        <h4>{newFilm}</h4>
      </div>
      <Grid item container lg={11} xl={11} md={11} className={classes.root2}>
        <AliceCarousel
          responsive={responsive}
          fadeOutAnimation
          startIndex={1}
          infinite
          mouseDragEnabled
        >
          {arrayCarousel.map((item) => {
            return (
              <div className={classes.divImg}>
                {" "}
                <img src={item.srcImg} className={classes.imgs} />
                {<h4>{nameArtist}</h4>}
              </div>
            );
          })}
        </AliceCarousel>
      </Grid>
    </Grid>
  );
}
