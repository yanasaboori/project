import React from "react";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import { GiBabyFace } from "react-icons/gi";
import { BiHomeAlt } from "react-icons/bi";
import { GiFilmSpool } from "react-icons/gi";
import { HiOutlineArrowCircleRight } from "react-icons/hi";
import { HiPlusSm } from "react-icons/hi";
import { Link } from "react-router-dom";
import useStyles from "./Styles";
const listDrawer = [
  {
    id: 1,
    icon: <BiHomeAlt size="28px" color="black" />,
    nameList: "خانه",
    to: "/home",
  },
  {
    id: 2,
    icon: <GiFilmSpool size="28px" color="black" />,
    nameList: "فیلم و سریال",
    to: "/filmSeries",
  },
  {
    id: 3,
    icon: <GiBabyFace size="28px" color="black" />,
    nameList: "کودک ",
    to: "/cartoon",
  },
  {
    id: 4,
    icon: <HiPlusSm size="28px" color="black" />,
    nameList: "درج اطلاعات",
    to: "/table",
  },
];
export default function Drawer({ handleDrawer, openDrawer }) {
  let classes = useStyles();
  return (
    <Grid container className={classes.root} justifyContent="center">
      {openDrawer ? (
        <Grid item container>
          {" "}
          <Grid item>
            <IconButton
              onClick={handleDrawer}
              color="primary"
              aria-label="upload picture"
              component="span"
            >
              <HiOutlineArrowCircleRight size="38px" color="black" />
            </IconButton>
          </Grid>
        </Grid>
      ) : null}

      <Grid
        item
        container
        xl={10}
        lg={10}
        md={10}
        sm={12}
        xs={12}
        justifyContent="center"
        className={classes.contlists}
      >
        <Grid item container xl={10} lg={10} md={11} sm={11} xs={12}>
          {listDrawer.map((item) => {
            return (
              <Grid
                item
                container
                justifyContent="flex-end"
                key={item.id}
                className={classes.conListMap}
              >
                <Link to={item.to} className={classes.link}>
                  {openDrawer && item.nameList}
                  &nbsp; &nbsp;
                  {item.icon}
                </Link>
              </Grid>
            );
          })}
        </Grid>
      </Grid>
    </Grid>
  );
}
