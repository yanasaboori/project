import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  root: {},
  img: {
    width: "95%",
    height: "240px",
    borderRadius: "10px",
  },
  "alice-carousel__prev-btn-wrapper": {
    display: "none !important",
  },
}));
