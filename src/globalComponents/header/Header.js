import React from "react";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import {
  useUserState,
  useUserDispatch,
  signOut,
} from "../../context/useContext";
import { useNavigate } from "react-router-dom";
// css
import useStyles from "./styles.js";
// icons
import { AiOutlineSearch } from "react-icons/ai";
import { AiOutlineUserDelete } from "react-icons/ai";
import { BiUserCircle } from "react-icons/bi";
import { HiMenu } from "react-icons/hi";

export default function Header({ handleDrawer, openDrawer }) {
  let authantication = useUserState();
  let distpath = useUserDispatch();
  let navigate = useNavigate();
  function handelExit() {
    return signOut(distpath, navigate, authantication);
  }

  let classes = useStyles();
  return (
    <Grid container className={classes.root}>
      <Grid
        item
        container
        className={classes.root2}
        justifyContent="space-between"
      >
        <Grid
          item
          container
          xl={1}
          lg={2}
          md={2}
          sm={4}
          xs={4}
          justifyContent="space-between"
        >
          <Grid item>
            {authantication.isAuthenticated ? (
              <AiOutlineUserDelete
                onClick={handelExit}
                size="30px"
                color="black"
                className={classes.iconLogOut}
              />
            ) : (
              <Link to="/login">
                <BiUserCircle
                  size="31px"
                  color="black"
                  className={classes.iconLogin}
                />
              </Link>
            )}
          </Grid>
          <Grid item>
            <Link to="/search">
              <AiOutlineSearch
                size="30px"
                color="black"
                className={classes.iconSearch}
              />
            </Link>
          </Grid>
        </Grid>
        {openDrawer === false ? (
          <Grid item>
            <HiMenu size="28px" onClick={handleDrawer} color="black" />
          </Grid>
        ) : null}
      </Grid>
    </Grid>
  );
}
